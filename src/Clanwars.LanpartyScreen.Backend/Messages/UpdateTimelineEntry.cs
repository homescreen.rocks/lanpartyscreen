using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record UpdateTimelineEntry
(
    [Required] DateTime? StartTime,
    string? Duration,
    // [Required]
    string Title,
    string? AdditionalInformation
);