namespace Clanwars.LanpartyScreen.Backend.Messages;

public record PictureEntryResponse
(
    uint Id,
    string Title,
    string Url,
    bool ShowOnStream 
);