using System;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record ShoutEntryResponse
(
    uint Id,
    string Name,
    string Text,
    DateTime PostedAt
);