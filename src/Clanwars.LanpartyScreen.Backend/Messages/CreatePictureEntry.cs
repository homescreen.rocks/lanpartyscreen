using System.ComponentModel.DataAnnotations;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record CreatePictureEntry
(
    [Required, StringLength(100, MinimumLength = 1)]
    string Title,
    [Required] string Image,
    bool ShowOnStream
);