using System;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record AnnouncementEntryResponse
(
    uint Id,
    string Headline,
    string Text,
    string IconClass,
    DateTime PostedAt,
    DateTime? UpdatedAt
);