using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record CreateWelcomeMessageEntry
(
    [Required] string LastName,
    [Required] string FirstName,
    [Required(AllowEmptyStrings = true)] string NickName,
    DateTime? CheckInTime
);
