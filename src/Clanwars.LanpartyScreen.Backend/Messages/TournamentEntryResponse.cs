using System;
using System.Collections.Generic;
using Clanwars.LanpartyScreen.Backend.Entities;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record TournamentEntryResponse
(
    uint Id,
    string Name,
    int PlayersPerTeam,
    int Teams,
    int TeamLimit,
    TournamentMode Mode,
    TournamentState State,
    DateTime StartTime,
    List<MatchResponse> Matches
);