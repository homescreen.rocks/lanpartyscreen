namespace Clanwars.LanpartyScreen.Backend.Messages;

public record UpdateAnnouncementEntry
(
    string Headline,
    string Text,
    string IconClass
);