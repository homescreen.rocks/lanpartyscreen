using System;
using System.ComponentModel.DataAnnotations;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record UpdateNewsEntry
(
    [Required, StringLength(100, MinimumLength = 1)]
    string Title,
    [Required, StringLength(100, MinimumLength = 1)]
    string Author,
    [Required] string Body,
    DateTime? PublishedAt
);