using System.Collections.Generic;

namespace Clanwars.LanpartyScreen.Backend.Messages;

public record MatchResponse
(
    uint Id,
    List<TeamResponse> Teams
);