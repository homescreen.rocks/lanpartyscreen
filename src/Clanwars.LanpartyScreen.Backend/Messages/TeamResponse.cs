namespace Clanwars.LanpartyScreen.Backend.Messages;

public record TeamResponse
(
    uint Id,
    string Name,
    int Points
);
