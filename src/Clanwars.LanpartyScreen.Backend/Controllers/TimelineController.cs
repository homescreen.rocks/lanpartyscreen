using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.LanpartyScreen.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class TimelineController : ControllerBase
{
    private readonly ITimelineRepository _timelineRepository;

    public TimelineController(ITimelineRepository timelineRepository)
    {
        _timelineRepository = timelineRepository;
    }

    // GET api/v1/timeline
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<Timeline>> GetAll()
    {
        var allElements = await _timelineRepository.FullTimelineAsync();
        return Ok(allElements);
    }

    // GET api/v1/timeline/{timelineEntryId}
    [HttpGet("{timelineEntryId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<TimelineEntryResponse>> Get([FromRoute] uint timelineEntryId)
    {
        var timelineEntry = await _timelineRepository.FindAsync(timelineEntryId);

        if (timelineEntry is null)
        {
            return NotFound();
        }

        return Ok(timelineEntry);
    }

    // POST api/v1/timeline
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<TimelineEntryResponse>> AddTimelineEntry([FromBody] CreateTimelineEntry newTimelineEntry)
    {
        var entry = newTimelineEntry.Adapt<TimelineEntry>();
        await _timelineRepository.InsertAsync(entry);
        return CreatedAtAction("Get", new {timelineEntryId = entry.Id}, entry.Adapt<TimelineEntryResponse>());
    }

    // PUT api/v1/timeline/{timelineEntryId}
    [HttpPut("{timelineEntryId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> UpdateTimelineEntry(
        [FromRoute] uint timelineEntryId,
        [FromBody] UpdateTimelineEntry updateTimelineEntry
    )
    {
        var oldEntry = await _timelineRepository.FindAsync(timelineEntryId);
        if (oldEntry == null)
        {
            return NotFound();
        }

        updateTimelineEntry.Adapt(oldEntry);

        await _timelineRepository.UpdateAsync(oldEntry);
        return NoContent();
    }

    // DELETE api/v1/timeline/{timelineEntryId}
    [HttpDelete("{timelineEntryId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteTimelineEntry([FromRoute] uint timelineEntryId)
    {
        var oldEntry = await _timelineRepository.FindAsync(timelineEntryId);

        if (oldEntry is null)
        {
            return NotFound();
        }

        await _timelineRepository.DeleteAsync(oldEntry);
        return NoContent();
    }
}
