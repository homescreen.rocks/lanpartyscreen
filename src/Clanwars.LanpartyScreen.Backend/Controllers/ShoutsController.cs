﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.LanpartyScreen.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class ShoutsController : ControllerBase
{
    private readonly IShoutRepository _shoutsRepository;
    private readonly IPipelinePublisher _publisher;

    public ShoutsController(IShoutRepository shoutsRepository, IPipelinePublisher publisher)
    {
        _shoutsRepository = shoutsRepository;
        _publisher = publisher;
    }

    // GET api/v1/shouts
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<ShoutEntryResponse>>> Get([FromQuery] uint? limit)
    {
        var result = limit is null
            ? await _shoutsRepository.AllAsync()
            : await _shoutsRepository.LastAsync(limit.Value);

        return Ok(result.Adapt<IEnumerable<ShoutEntryResponse>>());
    }

    // GET api/v1/shouts/{id}
    [HttpGet("{shoutId:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<ShoutEntryResponse>> GetItem([FromRoute] uint shoutId)
    {
        var item = await _shoutsRepository.FindAsync(shoutId);

        if (item is null)
        {
            return NotFound();
        }

        return Ok(item.Adapt<ShoutEntryResponse>());
    }

    // POST api/v1/shouts
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<ShoutEntryResponse>> Post([FromBody] CreateShoutEntry createShoutEntry)
    {
        var entry = createShoutEntry.Adapt<ShoutEntry>();
        await _shoutsRepository.InsertAsync(entry);
        await _publisher.AddPipelineEntryAsync(entry);

        return CreatedAtAction("GetItem", new {shoutId = entry.Id}, entry.Adapt<ShoutEntryResponse>());
    }

    // DELETE api/v1/shouts/5
    [HttpDelete("{shoutId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> Delete([FromRoute] uint shoutId)
    {
        if (!await _shoutsRepository.DoesItemExistAsync(shoutId))
        {
            return NotFound();
        }

        await _shoutsRepository.DeleteAsync(shoutId);

        return NoContent();
    }
}
