﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.LanpartyScreen.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class NewsController : ControllerBase
{
    private readonly INewsRepository _newsRepository;
    private readonly IPipelinePublisher _publisher;

    public NewsController(INewsRepository newsRepository, IPipelinePublisher publisher)
    {
        _newsRepository = newsRepository;
        _publisher = publisher;
    }

    // GET api/v1/news
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<NewsEntryResponse>>> Get([FromQuery] uint? limit)
    {
        var result = limit is null
            ? await _newsRepository.AllAsync()
            : await _newsRepository.LastAsync(limit.Value);

        return Ok(result.Adapt<IEnumerable<NewsEntryResponse>>());
    }

    // GET api/v1/news/{id}
    [HttpGet("{newsId}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<NewsEntryResponse>> GetItem([FromRoute] uint newsId)
    {
        var item = await _newsRepository.FindAsync(newsId);

        if (item is null)
        {
            return NotFound();
        }

        return Ok(item.Adapt<NewsEntryResponse>());
    }

    // POST api/v1/news
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created)]
    public async Task<ActionResult<NewsEntryResponse>> Post([FromBody] CreateNewsEntry newEntryMessage)
    {
        var entry = newEntryMessage.Adapt<NewsEntry>();
        await _newsRepository.InsertAsync(entry);
        await _publisher.AddPipelineEntryAsync(entry);

        return CreatedAtAction("GetItem", new {newsId = entry.Id}, entry.Adapt<NewsEntryResponse>());
    }

    // PUT api/v1/news/{id}
    [HttpPut("{newsId:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> Put(
        uint newsId,
        [FromBody] UpdateNewsEntry updateMsg
    )
    {
        var oldEntry = await _newsRepository.FindAsync(newsId);
        if (oldEntry == null)
        {
            return NotFound();
        }

        updateMsg.Adapt(oldEntry);

        await _newsRepository.UpdateAsync(oldEntry);

        return NoContent();
    }

    // DELETE api/v1/news/5
    [HttpDelete("{newsId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> Delete([FromRoute] uint newsId)
    {
        if (!await _newsRepository.DoesItemExistAsync(newsId))
        {
            return NotFound();
        }

        await _newsRepository.DeleteAsync(newsId);

        return NoContent();
    }
}
