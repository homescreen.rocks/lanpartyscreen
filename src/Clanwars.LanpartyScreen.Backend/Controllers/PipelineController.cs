﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Clanwars.LanpartyScreen.Backend.Controllers;

[Route("api/v1/[controller]")]
[ApiController]
public class PipelineController : ControllerBase
{
    private readonly IPipelineRepository _pipelineRepository;
    private readonly IPipelinePublisher _publisher;

    public PipelineController(IPipelineRepository pipelineRepository, IPipelinePublisher publisher)
    {
        _pipelineRepository = pipelineRepository;
        _publisher = publisher;
    }

    // GET api/v1/pipeline/modulestates
    [HttpGet("modulestates")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IList<PipelineModuleState>>> GetModuleStates()
    {
        return Ok(await _pipelineRepository.ActivationStatesAsync());
    }

    // POST api/v1/pipeline/run
    [HttpPost("{signal:regex(^(run|stop|skip)$)}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public IActionResult SetPipelineStatus([FromRoute] PipelineStatusAction signal)
    {
        switch (signal)
        {
            case PipelineStatusAction.Run:
                _publisher.PublisherAutoMode = true;
                break;
            case PipelineStatusAction.Stop:
                _publisher.PublisherAutoMode = false;
                break;
            case PipelineStatusAction.Skip:
                _publisher.Skip();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(signal), signal, null);
        }

        return NoContent();
    }

    /// <summary>
    /// Sets slide timeouts for a module.
    /// </summary>
    /// <remarks>
    /// Sample request:
    /// 
    ///     {
    ///         "timeoutInitial": 900,
    ///         "timeoutRecurring": 60
    ///     }
    /// 
    /// </remarks>
    /// <param name="module"></param>
    /// <param name="moduleSettings"></param>
    /// <returns></returns>
    [HttpPut("{module}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<PipelineModuleState>> SetModuleSettings([FromRoute] ModuleType module, [FromBody] [Required] PipelineModuleState moduleSettings)
    {
        var mod = await _pipelineRepository.SetModuleTimeoutsAsync(module, moduleSettings.TimeoutInitial,
            moduleSettings.TimeoutRecurring);
        return Ok(mod);
    }

    // PUT api/v1/pipeline/{module}/enable
    [HttpPut("{module}/{signal:regex(^(enable|disable)$)}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    public async Task<ActionResult> SetModuleState([FromRoute] ModuleType module, [FromRoute] PipelineModuleStatusAction signal)
    {
        switch (signal)
        {
            case PipelineModuleStatusAction.Enable:
                await _pipelineRepository.EnableModuleAsync(module);
                break;
            case PipelineModuleStatusAction.Disable:
                await _pipelineRepository.DisableModuleAsync(module);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(signal), signal, null);
        }

        return NoContent();
    }
}