﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LanpartyScreenBackend.Migrations
{
    public partial class addAnnouncementUpdatedAt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Announcements",
                type: "datetime(6)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Announcements");
        }
    }
}
