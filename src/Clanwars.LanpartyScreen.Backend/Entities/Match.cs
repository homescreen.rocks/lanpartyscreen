using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("Matches")]
public class Match
{
    [Key]
    public uint? Id { get; set; } = null;

    public List<Team> Teams { get; set; } = new();
}