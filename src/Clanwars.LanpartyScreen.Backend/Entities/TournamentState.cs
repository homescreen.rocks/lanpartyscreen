namespace Clanwars.LanpartyScreen.Backend.Entities;

public enum TournamentState
{
    Inactive = 0,
    Joining = 1,
    Running = 2,
    Finished = 3
}