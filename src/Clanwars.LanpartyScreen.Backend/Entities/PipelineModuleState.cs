using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("ModuleStates")]
public class PipelineModuleState
{
    [Key]
    public uint Id { get; set; }

    public ModuleType Module { get; set; }

    public bool IsEnabled { get; set; }

    [Required]
    [Range(30, 3600)]
    public uint TimeoutInitial { get; set; }

    [Required]
    [Range(30, 3600)]
    public uint TimeoutRecurring { get; set; }
}