using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("Teams")]
public class Team
{
    [Key]
    public uint? Id { get; set; }
        
    [Required]
    public string Name { get; set; } = null!;

    public int Points { get; set; } = 0;
}