using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("WelcomeMessages")]
public class WelcomeMessageEntry
{
    [Key]
    public uint Id { get; set; }

    [Required]
    public string LastName { get; set; } = null!;

    [Required]
    public string FirstName { get; set; } = null!;

    [Required]
    public string NickName { get; set; } = null!;

    public DateTime CheckInTime { get; set; }
}