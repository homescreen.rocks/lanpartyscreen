using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clanwars.LanpartyScreen.Backend.Entities;

[Table("Tournaments")]
public class TournamentEntry
{
    [Key]
    public uint Id { get; set; }

    [Required]
    [StringLength(255, MinimumLength = 1)]
    public string Name { get; set; } = null!;

    [Required]
    public int PlayersPerTeam { get; set; }

    [Required]
    public int Teams { get; set; }

    [Required]
    public int TeamLimit { get; set; }

    [Required]
    public TournamentMode Mode { get; set; }

    [Required]
    public TournamentState State { get; set; }

    [Required]
    public DateTime StartTime { get; set; }

    public List<Match> Matches { get; set; } = new();
}
