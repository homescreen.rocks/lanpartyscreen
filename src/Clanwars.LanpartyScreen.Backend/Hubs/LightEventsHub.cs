using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Microsoft.AspNetCore.SignalR;

namespace Clanwars.LanpartyScreen.Backend.Hubs;

public class LightEventsHub : Hub
{
    private IPipelinePublisher _publisher;

    public LightEventsHub(IPipelinePublisher publisher)
    {
        _publisher = publisher;
    }

    public override async Task OnConnectedAsync()
    {
        await base.OnConnectedAsync();
    }
}