﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;
using Clanwars.LanpartyScreen.Backend.Contexts;
using Clanwars.LanpartyScreen.Backend.Hubs;
using Clanwars.LanpartyScreen.Backend.Options;
using Clanwars.LanpartyScreen.Backend.Repositories;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Clanwars.TournamentManager.Lib.Extensions;
using Mapster;
using MapsterMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .AddDbContext<LanpartyScreenContext>(options =>
    {
        var dbConfig = builder.Configuration.GetSection(DatabaseOptions.Option).Get<DatabaseOptions>();
        options
            .UseMySql(
                dbConfig.GetConnectionString(),
                new MariaDbServerVersion(new Version(10, 9, 2)),
                sqlOptions => { sqlOptions.UseMicrosoftJson(); }
            );
    });

// Mapster configuration
var mapsterConfig = TypeAdapterConfig.GlobalSettings;
mapsterConfig.RequireExplicitMapping = true;
mapsterConfig.RequireDestinationMemberSource = true;
mapsterConfig.Scan(Assembly.GetExecutingAssembly());
mapsterConfig.Compile();

builder.Services.AddSingleton(mapsterConfig);
builder.Services.AddScoped<IMapper, ServiceMapper>();

builder.Services.AddControllers(options => { })
    .AddJsonOptions(options => JsonSerializerExtensions.JsonSerializerOptions(options.JsonSerializerOptions))
    .ConfigureApiBehaviorOptions(options =>
    {
        options.SuppressInferBindingSourcesForParameters = false;
        options.SuppressMapClientErrors = false;
    });

builder.Services
    .AddResponseCompression(opts =>
    {
        opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
            new[] {"application/octet-stream"});
    })
    .AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo {Title = "Lanparty Screen API", Version = "v1"});
        c.SupportNonNullableReferenceTypes();
        // Set the comments path for the Swagger JSON and UI.
        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
        c.IncludeXmlComments(xmlPath);
    });

builder.Services.AddCors();
builder.Services.AddSignalR(options => { options.EnableDetailedErrors = true; })
    .AddJsonProtocol(o =>
        {
            o.PayloadSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            o.PayloadSerializerOptions.IncludeFields = true;
        }
    );

// add DataRepositories and Services
builder.Services
    .AddScoped<INewsRepository, NewsRepository>()
    .AddScoped<IShoutRepository, ShoutRepository>()
    .AddScoped<IAnnouncementRepository, AnnouncementRepository>()
    .AddScoped<IPicturesRepository, PicturesRepository>()
    .AddScoped<ITournamentRepository, TournamentRepository>()
    .AddScoped<ITimelineRepository, TimelineRepository>()
    .AddScoped<IPipelineRepository, PipelineRepository>()
    .AddSingleton<IPipelinePublisher, PipelinePublisher>();

builder.Services.AddHealthChecks()
    .AddCheck("self", () => HealthCheckResult.Healthy("beamer backend service is running"))
    .AddDbContextCheck<LanpartyScreenContext>("database-connection", HealthStatus.Unhealthy);

var app = builder.Build();

app.UseResponseCompression();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => { c.SwaggerEndpoint("v1/swagger.json", "Lanparty v1"); });
}

if (app.Configuration.GetSection(DatabaseOptions.Option).Get<DatabaseOptions>().AutoApplyMigrations)
{
    await using var scope = app.Services.GetService<IServiceScopeFactory>()!.CreateAsyncScope();
    var db = scope.ServiceProvider.GetRequiredService<LanpartyScreenContext>();
    db.Database.Migrate();
}

app.UseCors();

app.UseRouting();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapHub<PipelineHub>("/pipeline");
    endpoints.MapHub<LightEventsHub>("/lightevents");
    endpoints.MapHealthChecks("/health");
});

// serve static files from wwwroot
app.UseDefaultFiles();
var fileExtensionContentTypeProvider = new FileExtensionContentTypeProvider
{
    Mappings = {[".webmanifest"] = "application/manifest+json"}
};
app.UseStaticFiles(new StaticFileOptions
{
    ContentTypeProvider = fileExtensionContentTypeProvider
});

app.Run();
