using Clanwars.LanpartyScreen.Backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.LanpartyScreen.Backend.Contexts;

public class LanpartyScreenContext : DbContext
{
    public LanpartyScreenContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<AnnouncementEntry> Announcements { get; set; } = null!;
    public DbSet<NewsEntry> News { get; set; } = null!;
    public DbSet<ShoutEntry> Shouts { get; set; } = null!;
    public DbSet<TimelineEntry> TimelineEntries { get; set; } = null!;
    public DbSet<PictureEntry> Pictures { get; set; } = null!;
    public DbSet<WelcomeMessageEntry> WelcomeMessages { get; set; } = null!;
    public DbSet<TournamentEntry> Tournaments { get; set; } = null!;
    public DbSet<Match> Matches { get; set; } = null!;
    public DbSet<Team> Teams { get; set; } = null!;
    public DbSet<PipelineModuleState> ModuleStates { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<PipelineModuleState>().HasIndex(s => s.Module).IsUnique();
    }
}