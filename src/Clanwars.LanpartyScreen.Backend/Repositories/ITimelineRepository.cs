using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public interface ITimelineRepository
{
    Task<Timeline> FullTimelineAsync();
    Task<TimelineEntry?> FindAsync(uint timelineEntryId);
    Task<TimelineEntry> InsertAsync(TimelineEntry entry);
    Task DeleteAsync(TimelineEntry entry);
    Task UpdateAsync(TimelineEntry entry);
}
