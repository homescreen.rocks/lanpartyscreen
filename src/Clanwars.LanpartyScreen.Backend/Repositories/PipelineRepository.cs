using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Contexts;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Services.Pipeline;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public class PipelineRepository : IPipelineRepository
{
    private readonly LanpartyScreenContext _context;

    public PipelineRepository(LanpartyScreenContext context)
    {
        _context = context;
    }

    public async Task<bool> IsModuleActivatedAsync(ModuleType type)
    {
        return await _context.ModuleStates.SingleOrDefaultAsync(s => s.Module == type && s.IsEnabled) != null;
    }

    public async Task<IList<PipelineModuleState>> ActivationStatesAsync()
    {
        return await _context.ModuleStates
            .OrderBy(m => m.Module)
            .ToListAsync();
    }

    public async Task EnableModuleAsync(ModuleType type)
    {
        var found = await _context.ModuleStates.FirstOrDefaultAsync(m => m.Module == type);
        if (found != null)
        {
            found.IsEnabled = true;
        }
        else
        {
            var e = new PipelineModuleState() {Module = type, IsEnabled = true};
            await _context.ModuleStates.AddAsync(e);
        }

        await _context.SaveChangesAsync();
    }

    public async Task DisableModuleAsync(ModuleType type)
    {
        var found = await _context.ModuleStates.FirstOrDefaultAsync(m => m.Module == type);
        if (found != null)
        {
            found.IsEnabled = false;
        }
        else
        {
            var e = new PipelineModuleState() {Module = type, IsEnabled = false};
            await _context.ModuleStates.AddAsync(e);
        }

        await _context.SaveChangesAsync();
    }

    public async Task<PipelineModuleState> SetModuleTimeoutsAsync(ModuleType type, uint timeoutInitial, uint timeoutRecurring)
    {
        var found = await _context.ModuleStates.FirstOrDefaultAsync(m => m.Module == type);
        if (found != null)
        {
            found.TimeoutInitial = timeoutInitial;
            found.TimeoutRecurring = timeoutRecurring;
            await _context.SaveChangesAsync();
            return found;
        }

        var newModule = new PipelineModuleState()
        {
            Module = type,
            IsEnabled = false,
            TimeoutInitial = timeoutInitial,
            TimeoutRecurring = timeoutRecurring
        };
        await _context.ModuleStates.AddAsync(newModule);
        await _context.SaveChangesAsync();
        return newModule;
    }


    public async Task<(uint Initial, uint Recurring)> GetTimeoutForModuleAsync(ModuleType module)
    {
        var mod = await _context.ModuleStates.FirstOrDefaultAsync(m => m.Module == module);
        var timeoutInitial = mod.TimeoutInitial == 0 ? GetDefaultTimeoutForModule(module, true) : mod.TimeoutInitial;
        var timeoutRecurring = mod.TimeoutRecurring == 0 ? GetDefaultTimeoutForModule(module, false) : mod.TimeoutRecurring;

        return (timeoutInitial, timeoutRecurring);
    }

    private static uint GetDefaultTimeoutForModule(ModuleType module, bool isInitial = false)
    {
        return module switch
        {
            ModuleType.News => isInitial ? 15 * 60u : 2 * 60u,
            ModuleType.Announcements => isInitial ? 15 * 60u : 2 * 60u,
            ModuleType.Timeline => 2 * 60,
            ModuleType.WelcomeMessage => 2 * 60,
            ModuleType.Tournaments => 1 * 60,
            ModuleType.Matches => 1 * 60,
            ModuleType.MatchResults => 1 * 60,
            ModuleType.MatchResult => 1 * 60,
            ModuleType.Shouts => isInitial ? 2 * 60u : 45,
            _ => 60
        };
    }
}
