using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Contexts;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public class PicturesRepository : IPicturesRepository
{
    private readonly LanpartyScreenContext _context;

    public PicturesRepository(LanpartyScreenContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<PictureEntry>> AllAsync()
    {
        return await _context.Pictures.ToListAsync();
    }

    public async Task<IEnumerable<PictureEntry>> LastAsync(uint limitLatest)
    {
        return await _context.Pictures.OrderByDescending(n => n.Id).Take((int) limitLatest).ToListAsync();
    }

    public async Task<PictureEntry?> FindAsync(uint id)
    {
        return await _context.Pictures.FirstOrDefaultAsync(n => n.Id == id);
    }

    public async Task<uint> InsertAsync(PictureEntry entry)
    {
        await _context.Pictures.AddAsync(entry);
        await _context.SaveChangesAsync();
        return entry.Id;
    }

    public async Task UpdateAsync(PictureEntry entry)
    {
        await _context.SaveChangesAsync();
    }

    public async Task DeleteAsync(uint id)
    {
        _context.Pictures.Remove(new PictureEntry {Id = id});
        await _context.SaveChangesAsync();
    }

    public async Task<PictureEntry> GetRandomAsync()
    {
        var count = await _context.Pictures.CountAsync();
        var rand = new Random();
        var i = rand.Next(0, count);
        var entry = await _context.Pictures.Where(p => p.ShowOnStream).Skip(i).Take(1).SingleOrDefaultAsync();
        if (entry is null)
        {
            throw new NoEntryInModuleException();
        }

        return entry;
    }
}
