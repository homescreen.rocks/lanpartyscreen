using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public interface IAnnouncementRepository
{
    Task<IEnumerable<AnnouncementEntry>> AllAsync();
    Task<IEnumerable<AnnouncementEntry>> LastAsync(int limitLatest);
    Task<bool> DoesItemExistAsync(uint id);
    Task<uint> InsertAsync(AnnouncementEntry entry);
    Task UpdateAsync(AnnouncementEntry entry);
    Task DeleteAsync(uint id);
    Task<AnnouncementEntry?> FindAsync(uint itemId);

    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    /// <exception cref="NoEntryInModuleException"></exception>
    Task<AnnouncementEntry> LatestAsync();
}