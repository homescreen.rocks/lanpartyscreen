using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Contexts;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Messages;
using Mapster;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public class TimelineRepository : ITimelineRepository
{
    private readonly LanpartyScreenContext _context;

    public TimelineRepository(LanpartyScreenContext context)
    {
        _context = context;
    }

    public async Task<Timeline> FullTimelineAsync()
    {
        var databaseElements = await _context.TimelineEntries.ToListAsync();
        var timeline = new Timeline();

        foreach (var databaseElement in databaseElements)
        {
            if (!timeline.ContainsKey(databaseElement.StartTime))
            {
                timeline.Add(databaseElement.StartTime, new List<TimelineEntryResponse>());
            }

            timeline[databaseElement.StartTime].Add(databaseElement.Adapt<TimelineEntryResponse>());
        }

        return timeline;
    }

    public async Task UpdateAsync(TimelineEntry entry)
    {
        entry.StartTime = entry.StartTime.AddSeconds(-entry.StartTime.Second);
        _context.TimelineEntries.Update(entry);
        await _context.SaveChangesAsync();
    }

    public async Task<TimelineEntry?> FindAsync(uint timelineEntryId)
    {
        return await _context.TimelineEntries.FirstOrDefaultAsync(t => t.Id == timelineEntryId);
    }

    public async Task<TimelineEntry> InsertAsync(TimelineEntry entry)
    {
        entry.StartTime = entry.StartTime.AddSeconds(-entry.StartTime.Second);
        await _context.TimelineEntries.AddAsync(entry);
        await _context.SaveChangesAsync();
        return entry;
    }

    public async Task DeleteAsync(TimelineEntry entry)
    {
        _context.TimelineEntries.Remove(entry);
        await _context.SaveChangesAsync();
    }
}
