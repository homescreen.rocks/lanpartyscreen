using System.Collections.Generic;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Entities;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public interface INewsRepository : IRandomlyAccessible<NewsEntry>
{
    Task<bool> DoesItemExistAsync(uint id);
    Task<IEnumerable<NewsEntry>> AllAsync();
    Task<IEnumerable<NewsEntry>> LastAsync(uint limitLatest);
    Task<NewsEntry?> FindAsync(uint id);
    Task<uint> InsertAsync(NewsEntry entry);
    Task UpdateAsync(NewsEntry entry);
    Task DeleteAsync(uint id);
}