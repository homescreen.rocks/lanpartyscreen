using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Contexts;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public class ShoutRepository : IShoutRepository
{
    private readonly LanpartyScreenContext _context;

    public ShoutRepository(LanpartyScreenContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<ShoutEntry>> AllAsync()
    {
        return await _context.Shouts.OrderByDescending(s => s.PostedAt).ToListAsync();
    }

    public async Task<IEnumerable<ShoutEntry>> LastAsync(uint limitLatest)
    {
        return await _context.Shouts.OrderByDescending(s => s.PostedAt).Take((int) limitLatest).ToListAsync();
    }

    public async Task<bool> DoesItemExistAsync(uint id)
    {
        return await _context.Shouts.CountAsync(s => s.Id == id) > 0;
    }

    public async Task<ShoutEntry?> FindAsync(uint id)
    {
        return await _context.Shouts.FirstOrDefaultAsync(s => s.Id == id);
    }

    public async Task InsertAsync(ShoutEntry shout)
    {
        await _context.Shouts.AddAsync(shout);
        await _context.SaveChangesAsync();
    }

    public async Task DeleteAsync(uint id)
    {
        _context.Shouts.Remove(new ShoutEntry {Id = id});
        await _context.SaveChangesAsync();
    }

    public async Task<ShoutEntry> GetRandomAsync()
    {
        var allEntries = await _context.Shouts.ToListAsync();
        if (allEntries.Count < 1)
        {
            throw new NoEntryInModuleException();
        }

        var rnd = new Random();
        var entry = allEntries[rnd.Next(allEntries.Count)];

        return entry;
    }
}
