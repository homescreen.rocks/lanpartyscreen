using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.LanpartyScreen.Backend.Contexts;
using Clanwars.LanpartyScreen.Backend.Entities;
using Clanwars.LanpartyScreen.Backend.Helpers.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.LanpartyScreen.Backend.Repositories;

public class TournamentRepository : ITournamentRepository
{
    private readonly LanpartyScreenContext _context;

    public TournamentRepository(LanpartyScreenContext context)
    {
        _context = context;
    }

    public async Task<IList<TournamentEntry>> AllAsync()
    {
        return await _context.Tournaments
            .Include(t => t.Matches)
            .ThenInclude(m => m.Teams)
            .ToListAsync();
    }

    public async Task<IList<TournamentEntry>> LastAsync(uint limitLatest)
    {
        return await _context.Tournaments
            .Include(t => t.Matches)
            .ThenInclude(m => m.Teams).OrderByDescending(t => t.StartTime).Take((int) limitLatest).ToListAsync();
    }

    public async Task<bool> DoesItemExistAsync(uint tournamentId)
    {
        return await _context.Tournaments.CountAsync(t => t.Id == tournamentId) > 0;
    }

    public async Task<TournamentEntry?> FindAsync(uint tournamentId)
    {
        return await _context.Tournaments
            .Include(t => t.Matches)
            .ThenInclude(m => m.Teams)
            .FirstOrDefaultAsync(t => t.Id == tournamentId);
    }

    public async Task InsertAsync(TournamentEntry tournament)
    {
        await _context.Tournaments.AddAsync(tournament);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateAsync(TournamentEntry tournament)
    {
        await _context.SaveChangesAsync();
    }

    public async Task DeleteAsync(TournamentEntry tournament)
    {
        foreach (var match in tournament.Matches)
        {
            foreach (var team in match.Teams)
            {
                _context.Teams.Remove(team);
            }
            
            _context.Matches.Remove(match);
        }

        _context.Tournaments.Remove(tournament);
        await _context.SaveChangesAsync();
    }

    public async Task<TournamentEntry> GetRandomAsync()
    {
        var allEntries = await AllAsync();
        if (allEntries.Count < 1)
        {
            throw new NoEntryInModuleException();
        }

        var rnd = new Random();
        var entry = allEntries[rnd.Next(allEntries.Count)];

        return entry;
    }
}
