namespace Clanwars.LanpartyScreen.Backend.Services.Pipeline;

public enum ModuleType
{
    News,
    Announcements,
    Shouts,
    Tournaments,
    Matches,
    MatchResults,
    MatchResult,
    Pictures,
    Timeline,
    WelcomeMessage
}