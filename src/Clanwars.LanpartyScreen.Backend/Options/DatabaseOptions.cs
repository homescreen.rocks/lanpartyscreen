namespace Clanwars.LanpartyScreen.Backend.Options;

public class DatabaseOptions
{
    public const string Option = "Database";

    public string Host { get; set; } = null!;
    public int Port { get; set; }
    public string Username { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string Database { get; set; } = null!;

    public bool AutoApplyMigrations { get; set; }

    public string GetConnectionString()
    {
        return $"Host={Host};Port={Port};Username={Username};Password={Password};Database={Database};";
    }
}
