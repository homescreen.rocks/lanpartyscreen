# lanpartyscreen-backend
This is the [dotnet core](https://www.microsoft.com/net/learn/get-started-with-dotnet-tutorial) based backend for our lanparty beamer screen. It's used at the [Clanwars Lanparty](hsf-clanwars.de) .
## Prequisites
* [dotnet CLI 6](https://www.microsoft.com/net/learn/get-started-with-dotnet-tutorial) 

## Build & Use
* `dotnet run`

## API documentation
* Swagger is integrated in `Development` builds
* access it via http://localhost:5000/swagger
