export class Picture {
  id: number;
  title: string;
  url: string;
  showOnStream: boolean;
}
