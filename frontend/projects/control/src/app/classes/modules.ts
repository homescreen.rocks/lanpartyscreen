export class ModulesList {
  private readonly _modules: Array<Module>;

  constructor() {
    this._modules = [
      {module: 'News', name: 'News', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'Announcements', name: 'Announcements', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'Shouts', name: 'Shouts', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'Tournaments', name: 'Tournaments', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'Matches', name: 'Upcoming Matches', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'MatchResults', name: 'Finished Matches', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'MatchResult', name: 'Match Results', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'Pictures', name: 'Pictures', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'Timeline', name: 'Timeline', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0},
      {module: 'WelcomeMessage', name: 'Welcome Message', isEnabled: false, active: false, timeoutInitial: 0, timeoutRecurring: 0}
    ];
  }

  get modules(): Array<Module> {
    return this._modules;
  }
}

export interface Module {
  module: string;
  name: string;
  isEnabled: boolean;
  active: boolean;
  timeoutInitial: number;
  timeoutRecurring: number;
}
