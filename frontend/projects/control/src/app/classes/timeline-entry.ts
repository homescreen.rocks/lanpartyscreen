export class TimelineEntry {
  public id: number;
  public startTime: Date;
  public duration: string;
  public title: string;
  public additionalInformation: string | null;
}
