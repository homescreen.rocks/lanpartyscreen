import {Injectable} from '@angular/core';
import {PipelineService} from './pipeline.service';
import {Module, ModulesList} from '../classes/modules';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ModulesService {
  private readonly _modules: Array<Module>;

  constructor(private pipeline: PipelineService, private http: HttpClient) {
    this._modules = (new ModulesList()).modules;
    pipeline.activeModule.subscribe(activeModule => {
      this._modules.forEach((value) => {
        value.active = value.module === activeModule;
      });
    });
    http.get<Module[]>('/api/v1/Pipeline/modulestates').subscribe(result => {
      result.map(module => {
        this._modules.forEach((value) => {
          if (value.module === module.module) {
            value.isEnabled = module.isEnabled;
            value.timeoutInitial = module.timeoutInitial;
            value.timeoutRecurring = module.timeoutRecurring;
          }
        });
      });
    });
  }

  get modules() {
    return this._modules;
  }

  activateModule(module: Module) {
    this.http.put('/api/v1/Pipeline/' + module.module + '/' + (module.isEnabled ? 'disable' : 'enable'), null).subscribe(
        () => module.isEnabled = !module.isEnabled
    );
  }

  setModuleTimeouts(module: Module, timeoutInitial: number, timeoutRecurring: number) {
    this.http.put('/api/v1/Pipeline/' + module.module, {timeoutInitial: timeoutInitial, timeoutRecurring: timeoutRecurring}).subscribe(
        (mod: Module) => {
          module.timeoutInitial = mod.timeoutInitial;
          module.timeoutRecurring = mod.timeoutRecurring;
        });
  }
}
