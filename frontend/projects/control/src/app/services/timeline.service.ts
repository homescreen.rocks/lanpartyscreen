import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TimelineEntry} from '../classes/timeline-entry';

@Injectable({
  providedIn: 'root'
})
export class TimelineService {

  constructor(private http: HttpClient) {
  }

  getTimeline(): Observable<Map<Date, TimelineEntry[]>> {
    return this.http.get<Map<Date, TimelineEntry[]>>('/api/v1/timeline');
  }

  deleteEntry(id: number) {
    return this.http.delete('/api/v1/timeline/' + id);
  }

  addEntry(entry: TimelineEntry) {
    return this.http.post <TimelineEntry>('/api/v1/timeline', entry);
  }

  updateEntry(entry: TimelineEntry) {
    return this.http.put('/api/v1/timeline/' + entry.id, entry);
  }
}
