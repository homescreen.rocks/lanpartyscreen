import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Picture} from '../classes/picture';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StaticPicturesService {

  constructor(private http: HttpClient) {
  }

  getAllPictures(): Observable<Array<Picture>> {
    return this.http.get<Array<Picture>>('/api/v1/Pictures');
  }

  deletePicture(id: number) {
    return this.http.delete('/api/v1/Pictures/' + id);
  }

  addPicture(fileBase64: any, title: string, showOnBeamer: boolean) {
    return this.http.post('/api/v1/Pictures', {image: fileBase64, title: title, showOnBeamer: showOnBeamer});
  }

  activatePicture(id: number, timeout: number) {
    return this.http.post('/api/v1/Pictures/' + id + '/activate/' + timeout, null);
  }

  enablePictureForStream(id: number) {
    return this.http.put('/api/v1/Pictures/' + id + '/show-on-stream', null);
  }

  disablePictureForStream(id: number) {
    return this.http.put('/api/v1/Pictures/' + id + '/hide-on-stream', null);
  }
}
