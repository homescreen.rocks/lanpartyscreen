import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Announcement} from '../classes/announcement';

@Injectable({
  providedIn: 'root'
})
export class AnnouncementsService {

  constructor(private http: HttpClient) {
  }

  getAllAnnouncements(): Observable<Announcement[]> {
    return this.http.get<Announcement[]>('/api/v1/announcements');
  }

  deleteAnnouncement(id: number) {
    return this.http.delete('/api/v1/announcements/' + id);
  }

  addAnnouncement(announcement: Announcement) {
    if (!announcement.iconClass) {
      announcement.iconClass = 'far fa-comment-alt';
    }
    return this.http.post<Announcement>('/api/v1/announcements', announcement);
  }

  activateAnnouncement(id: number, timeout: number) {
    return this.http.post<Announcement>('/api/v1/announcements/' + id + '/activate/' + timeout, null);
  }

  updateAnnouncement(announcement: Announcement) {
    return this.http.put('/api/v1/announcements/' + announcement.id, announcement);
  }
}
