import {Component, OnInit} from '@angular/core';
import {TimelineEntry} from '../../../classes/timeline-entry';
import {TimelineService} from '../../../services/timeline.service';
import {faPencilAlt} from '@fortawesome/free-solid-svg-icons';
import {FormControl, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  faPencilAlt = faPencilAlt;
  timeline: Map<Date, TimelineEntry[]>;

  timelineEntryForm = new FormGroup({
    id: new FormControl(''),
    startTime: new FormControl(''),
    duration: new FormControl(''),
    title: new FormControl(''),
    additionalInformation: new FormControl(''),
  });

  uploadingMessage;

  constructor(private service: TimelineService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.service.getTimeline().subscribe(
      (timeline => this.timeline = timeline)
    );
    this.timelineEntryForm.valueChanges.subscribe(() => {
    });
  }

  public openModal(modal, te: TimelineEntry) {
    console.log(te);
    this.timelineEntryForm.patchValue(te);
    this.modalService.open(modal, {size: 'lg'}).result.then(() => {
      this.service.getTimeline().subscribe(timeline => this.timeline = timeline);
    }, () => {
    });
  }

  public openAddModal(modal) {
    this.timelineEntryForm.reset();
    this.modalService.open(modal, {size: 'lg'}).result.then(() => {
      this.service.getTimeline().subscribe(timeline => this.timeline = timeline);
    }, () => {
    });
  }

  deleteElement(entryId: number, modal) {
    console.log('clicked on delete');
    this.service.deleteEntry(entryId).subscribe(() => {
      this.service.getTimeline().subscribe(timeline => this.timeline = timeline);
      this.timelineEntryForm.reset();
      modal.close();
    });
  }

  saveTimelineEntry(modal) {
    this.uploadingMessage = 'Uploading.... please wait...';
    const teForm = this.timelineEntryForm.value;
    // delete teForm.id;
    this.service.addEntry(teForm).subscribe(() => {
      this.uploadingMessage = null;
      this.timelineEntryForm.reset();
      modal.close('closed after upload');
    });
  }

  updateTimelineEntry(modal) {
    this.uploadingMessage = 'Uploading.... please wait...';
    this.service.updateEntry(this.timelineEntryForm.value).subscribe(() => {
      this.uploadingMessage = null;
      this.timelineEntryForm.reset();
      modal.close('closed after upload');
    });
  }
}
