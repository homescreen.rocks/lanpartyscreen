import {Component, OnInit} from '@angular/core';
import {faPause, faPlay} from '@fortawesome/free-solid-svg-icons';
import {PipelineService} from '../../../services/pipeline.service';

@Component({
  selector: 'app-autoplay',
  templateUrl: './autoplay.component.html',
  styleUrls: ['./autoplay.component.scss']
})
export class AutoplayComponent implements OnInit {
  faPause = faPause;
  faPlay = faPlay;

  autoPlayOn: boolean;

  constructor(private pipeline: PipelineService) {
  }

  ngOnInit() {
    this.pipeline.autoPlayState.subscribe(state => this.autoPlayOn = state);
  }

  toggleAutoPlay() {
    this.pipeline.setAutoPlayState(!this.autoPlayOn);
  }

}
