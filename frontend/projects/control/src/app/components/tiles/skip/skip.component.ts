import {Component, OnInit} from '@angular/core';
import {faForward} from '@fortawesome/free-solid-svg-icons';
import {PipelineService} from '../../../services/pipeline.service';

@Component({
  selector: 'app-skip',
  templateUrl: './skip.component.html',
  styleUrls: ['./skip.component.scss']
})
export class SkipComponent implements OnInit {
  faForward = faForward;
  autoPlayOn: boolean;

  constructor(private pipeline: PipelineService) {
  }

  ngOnInit() {
    this.pipeline.autoPlayState.subscribe(state => this.autoPlayOn = state);
  }

  skip() {
    this.pipeline.skip();
  }

}
