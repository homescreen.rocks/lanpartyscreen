import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModuleActivationComponent } from './module-activation.component';

describe('ModuleActivationComponent', () => {
  let component: ModuleActivationComponent;
  let fixture: ComponentFixture<ModuleActivationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleActivationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleActivationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
