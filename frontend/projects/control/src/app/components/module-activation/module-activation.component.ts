import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ModulesService} from '../../services/modules.service';
import {Module} from '../../classes/modules';
import {faPause, faPlay, faVideo, faWrench} from '@fortawesome/free-solid-svg-icons';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-module-activation',
  templateUrl: './module-activation.component.html',
  styleUrls: ['./module-activation.component.scss']
})
export class ModuleActivationComponent implements OnInit {
  faVideo = faVideo;
  faWrench = faWrench;
  faPlay = faPlay;
  faPause = faPause;
  public modules: Array<Module>;

  constructor(private modulesService: ModulesService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.modules = this.modulesService.modules;
  }

  toggleModuleState(mod: Module) {
    this.modulesService.activateModule(mod);
  }

  openTimeoutModal(event, module: Module) {
    const modalInstance = this.modalService.open(AppModuleTimeoutsModalComponent, {ariaLabelledBy: 'modal-basic-title'});
    modalInstance.componentInstance.module = module;
    modalInstance.result.then((mod: Module) => {
      this.modulesService.setModuleTimeouts(module, mod.timeoutInitial, mod.timeoutRecurring);
      event.target.blur();
    }, (reason) => {
      event.target.blur();
    });
    event.stopPropagation();
  }
}

@Component({
  selector: 'app-modal-module-timeouts',
  template: `
    <div class="modal-header">
      <h4 class="modal-title" id="modal-basic-title">Change timeouts for {{mod.name}}</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form>
        <div class="form-group">
          <label for="timeoutInitial">Timeout initial</label>
          <div class="input-group">
            <input id="timeoutInitial"
                   min="30" max="3600"
                   class="form-control"
                   ngbAutofocus
                   [(ngModel)]="mod.timeoutInitial"
                   name="timeoutInitial"
                   type="number">
          </div>
        </div>
        <div class="form-group">
          <label for="timeoutRecurring">Timeout recurring</label>
          <div class="input-group">
            <input id="timeoutRecurring"
                   min="30" max="3600"
                   class="form-control"
                   ngbAutofocus
                   [(ngModel)]="mod.timeoutRecurring"
                   name="timeoutRecurring"
                   type="number">
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" (click)="activeModal.close(mod)">Save</button>
    </div>
  `
})
export class AppModuleTimeoutsModalComponent implements OnInit {
  @Input() module: Module;

  mod: Module;

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
    if (this.module) {
      this.mod = Object.assign({}, this.module);
    }
  }

}
