import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {NgbAlertModule, NgbModalModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AppModuleTimeoutsModalComponent, ModuleActivationComponent} from './components/module-activation/module-activation.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AutoplayComponent} from './components/tiles/autoplay/autoplay.component';
import {StaticPicturesComponent} from './components/tiles/static-pictures/static-pictures.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SignalrErrorModalComponent} from './services/pipeline.service';
import {SkipComponent} from './components/tiles/skip/skip.component';
import {AnnouncementsComponent} from './components/tiles/announcements/announcements.component';
import {TimelineComponent} from './components/tiles/timeline/timeline.component';
import {MarkdownModule} from 'ngx-markdown';

@NgModule({
  declarations: [
    AppComponent,
    ModuleActivationComponent,
    AppModuleTimeoutsModalComponent,
    AutoplayComponent,
    StaticPicturesComponent,
    SignalrErrorModalComponent,
    SkipComponent,
    AnnouncementsComponent,
    TimelineComponent,
  ],
  entryComponents: [
    SignalrErrorModalComponent,
    AppModuleTimeoutsModalComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FontAwesomeModule,
    NgbModalModule,
    NgbAlertModule,
    FormsModule,
    ReactiveFormsModule,
    MarkdownModule.forRoot({loader: HttpClient}),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
