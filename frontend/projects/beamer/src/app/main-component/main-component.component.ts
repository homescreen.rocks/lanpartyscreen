import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {ModuleType} from '../classes/moduleType';
import {PipelineEntry} from '../classes/pipeline-entry';
import {PipelineService} from '../services/pipeline.service';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.scss'],
  animations: [

    trigger('container', [

      // the "in" style determines the "resting" state of the element when it is visible.
      state('fadeIn', style({opacity: 1})),
      state('fadeOut', style({opacity: 0})),

      // fade in when created. this could also be written as transition('void => *')
      transition('* => fadeOut', [
        animate('600ms ease-in-out', style({opacity: 0}))
      ]),
      transition('* => fadeIn', [
        animate('600ms ease-in-out', style({opacity: 1}))
      ]),

      // // fade out when destroyed. this could also be written as transition('void => *')
      // transition(':leave',
      //   animate(600, style({opacity: 0})))
    ])
  ]
})
export class MainComponentComponent implements OnInit {

  public autoPlayState: boolean;
  public entries: PipelineEntry[] = [];

  public entry: PipelineEntry;

  public moduleTypes: typeof ModuleType = ModuleType;

  public fadingState: 'fadeIn' | 'fadeOut';

  showDebug = environment.showDebugJson;

  constructor(private pipe: PipelineService) {
  }

  toggle() {
    if (this.fadingState === 'fadeOut') {
      this.fadingState = 'fadeIn';
      return;
    }
    this.fadingState = 'fadeOut';
  }

  ngOnInit(): void {
    this.pipe.autoPlayState.subscribe(state => this.autoPlayState = state);
    this.pipe.activeModule.subscribe(nextEntry => {
      if (!nextEntry) {
        return;
      }
      this.fadingState = 'fadeOut';
      // if (this.entry) {
      //   return;
      // }
      setTimeout(() => {
        this.entry = null;
      }, 600);

      setTimeout(() => {
        this.entry = nextEntry;
      }, 700);

      setTimeout(() => {
        this.fadingState = 'fadeIn';
      }, 1200);

      // setTimeout(() => {
      //   if (this.entries.length > 1) {
      //     this.entries.splice(0, 1);
      //   }
      // }, 610);
    });
  }

  private pushToEntries(next: PipelineEntry) {
    this.entries.push(next);
    console.log(this.entries);
  }
}
