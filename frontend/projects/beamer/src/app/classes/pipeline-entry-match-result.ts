import {PipelineEntryAbstract} from './pipeline-entry-abstract';
import {ModuleType} from './moduleType';
import {MatchResult} from './match-result';

export class PipelineEntryMatchResult extends PipelineEntryAbstract {
  type = ModuleType.MatchResult;
  payload: MatchResult;
}
