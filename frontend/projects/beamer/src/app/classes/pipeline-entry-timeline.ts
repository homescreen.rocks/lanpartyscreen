import {ModuleType} from './moduleType';
import {PipelineEntryAbstract} from './pipeline-entry-abstract';
import {TimelineEntry} from './timeline-entry';

export class PipelineEntryTimeline extends PipelineEntryAbstract {
  type = ModuleType.Timeline;
  payload: Map<Date, TimelineEntry[]>;
}
