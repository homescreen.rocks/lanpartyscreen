import {Match} from './match';
import {Tournament} from './tournament';

export class MatchResult {
  match: Match;
  tournament: Tournament;
}
