export class TimelineEntry {
  id: number;
  startTime: Date;
  duration: string;
  title: string;
  additionalInformation: string;
}
