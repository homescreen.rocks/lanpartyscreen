import {PipelineEntryAbstract} from './pipeline-entry-abstract';
import {ModuleType} from './moduleType';
import {Tournament} from './tournament';

export class PipelineEntryTournament extends PipelineEntryAbstract {
  type = ModuleType.Tournaments;
  payload: Tournament[];
}
