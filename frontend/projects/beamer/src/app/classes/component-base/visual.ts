export interface Visual {
  videoUrl: string;
  accentColor: string;
}
