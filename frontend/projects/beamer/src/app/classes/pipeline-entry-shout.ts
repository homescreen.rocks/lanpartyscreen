import {PipelineEntryAbstract} from './pipeline-entry-abstract';
import {ModuleType} from './moduleType';
import {Shout} from './shout';

export class PipelineEntryShout extends PipelineEntryAbstract {
  type = ModuleType.Shouts;
  payload: Shout;
}
