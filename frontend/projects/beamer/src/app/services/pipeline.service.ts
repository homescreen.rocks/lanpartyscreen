import {Component, Injectable, Input} from '@angular/core';
import {BehaviorSubject, Observable, timer} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {PipelineEntry} from '../classes/pipeline-entry';
import {PipelineEntryTimeline} from '../classes/pipeline-entry-timeline';
import {map, take, tap} from 'rxjs/operators';
import {HubConnection, HubConnectionBuilder} from '@microsoft/signalr';

@Injectable({
  providedIn: 'root',
})
export class PipelineService {
  private connection: HubConnection;
  private _activeModule: BehaviorSubject<PipelineEntry> = new BehaviorSubject<PipelineEntry>(null);
  private autoplayOn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

  constructor(private http: HttpClient, private dialog: MatDialog) {
    this.connection = new HubConnectionBuilder()
      .withUrl('/pipeline')
      .build();

    this.connection.onclose(err => {
      this.openWbesocketErrorModal(err);
    });

    this.connection.start().catch(err => {
      this.openWbesocketErrorModal(err);
    });

    this.connection.on('publish', (entry: PipelineEntry) => {
      if (entry === null) {
        console.warn('Pipeline seems to be stopped!');
      } else {
        console.log(entry);
        if (entry instanceof PipelineEntryTimeline) {
          console.log('got an timeline message');
        }
        this._activeModule.next(entry);
      }
    });

    this.connection.on('autoplay', (state: string) => {
      this.autoplayOn.next(state === 'on');
    });
  }

  private openWbesocketErrorModal(err) {
    const modalRef = this.dialog.open(SignalrErrorModalComponent, {disableClose: true, width: '400px'});
    modalRef.componentInstance.error = err;
  }

  get activeModule(): Observable<PipelineEntry> {
    return this._activeModule.asObservable();
  }

  get autoPlayState(): Observable<boolean> {
    return this.autoplayOn.asObservable();
  }

  setAutoPlayState(state: boolean) {
    this.http.post('/api/v1/Pipeline/' + (state ? 'run' : 'stop'), null).subscribe();
  }

  skip() {
    this.http.post('/api/v1/Pipeline/skip', null).subscribe();
  }
}

@Component({
  selector: 'app-signalr-error-modal',
  template: `
    <h1 mat-dialog-title>Error</h1>
    <div mat-dialog-content>
      <p>Websocket connection was lost!</p>
      <p>{{error}}</p>
      <p>Trying to reconnect in {{retrySeconds}} seconds.</p>
    </div>
    <div mat-dialog-actions>
      <button mat-raised-button (click)="reload()">Refresh now</button>
    </div>
  `,
})
export class SignalrErrorModalComponent {
  @Input() error;

  retrySeconds: number;

  constructor(public dialogRef: MatDialogRef<SignalrErrorModalComponent>) {
    this.startCountdown();
  }

  public reload() {
    window.location.reload();
  }

  private startCountdown() {
    const timeout = 10;
    this.retrySeconds = timeout;
    const countdown = timer(0, 1000);
    countdown.pipe(
      map(i => timeout - i),
      tap(v => this.retrySeconds = v),
      take(timeout + 1),
    ).subscribe(() => {
    }, () => {
    }, () => {
      this.retryConnection();
    });
  }

  private retryConnection() {
    const testConnection = new HubConnectionBuilder()
      .withUrl('/pipeline')
      .build();

    testConnection.onclose(err => {
      this.startCountdown();
    });

    testConnection.start()
      .then(() => {
        this.reload();
      })
      .catch(err => {
        this.startCountdown();
      });
  }
}
