import {Component, Input, OnInit} from '@angular/core';
import {faTrophy} from '@fortawesome/free-solid-svg-icons/faTrophy';
import {Match} from '../../classes/match';

@Component({
  selector: 'app-match-row',
  templateUrl: './match-row.component.html',
  styleUrls: ['./match-row.component.scss']
})
export class MatchRowComponent implements OnInit {

  @Input() match: Match;
  @Input() hasResult = false;

  faTrophy = faTrophy;

  constructor() {
  }

  ngOnInit() {
  }

}
