import {Component, Input, OnInit} from '@angular/core';
import {faGamepad} from '@fortawesome/free-solid-svg-icons/faGamepad';
import {faPause} from '@fortawesome/free-solid-svg-icons/faPause';
import {faPencilAlt} from '@fortawesome/free-solid-svg-icons/faPencilAlt';
import {faPlay} from '@fortawesome/free-solid-svg-icons/faPlay';
import {faTrophy} from '@fortawesome/free-solid-svg-icons/faTrophy';
import {Tournament} from '../../classes/tournament';
import {TournamentState} from '../../classes/tournament-state';

@Component({
  selector: 'app-tournament-row',
  templateUrl: './tournament-row.component.html',
  styleUrls: ['./tournament-row.component.scss']
})
export class TournamentRowComponent implements OnInit {

  @Input() tournament: Tournament;

  faGamepad = faGamepad;
  faTrophy = faTrophy;

  tournamentStates = TournamentState;

  faPlay = faPlay;
  faPause = faPause;
  faPencilAlt = faPencilAlt;

  constructor() {
  }

  ngOnInit() {
  }


}
