import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FinishedMatchesComponent } from './finished-matches.component';

describe('FinishedMatchesComponent', () => {
  let component: FinishedMatchesComponent;
  let fixture: ComponentFixture<FinishedMatchesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishedMatchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedMatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
