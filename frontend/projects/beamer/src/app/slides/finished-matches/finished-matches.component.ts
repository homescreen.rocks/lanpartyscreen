import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {environment} from '../../../environments/environment';
import {ComponentBase} from '../../classes/component-base/component-base';
import {Match} from '../../classes/match';
import {PipelineEntryTournament} from '../../classes/pipeline-entry-tournament';
import {Tournament} from '../../classes/tournament';

@Component({
  selector: 'app-finished-matches',
  templateUrl: './finished-matches.component.html',
  styleUrls: ['./finished-matches.component.scss'],

})
export class FinishedMatchesComponent extends ComponentBase implements OnChanges {

  @Input() message: PipelineEntryTournament;
  tournament: Tournament;
  matchList: Match[] = [];


  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.tournament = changes['message'].currentValue.payload;
    // only completed matches
    let list = <Match[]>this.tournament.matches.filter(match => {
      return match.teams.filter(team => {
        return team.points !== 0;
      }).length !== 0;
    });
    list = list.sort((match1, match2) => {
      return match2.id - match1.id;
    });
    setTimeout(() => {
      this.startTableAnimation(list);
    }, environment.enterAnimationTimeout);
  }

  private startTableAnimation(list: Match[]) {
    list.forEach((match: Match, index) => {
      if (index < 8) {
        setTimeout(() => {
          this.matchList.push(match);
        }, 500 * (index + 1) / 3);
      }
    });
  }

}
