import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnInit} from '@angular/core';
import {ComponentBase} from '../../classes/component-base/component-base';
import {PipelineEntryMatchResult} from '../../classes/pipeline-entry-match-result';
import {Team} from '../../classes/team';

@Component({
  selector: 'app-match-result',
  templateUrl: './match-result.component.html',
  styleUrls: ['./match-result.component.scss'],
  animations: [
    trigger('versus', [
      state('true', style({opacity: 1})),
      state('false', style({opacity: 0})),
      transition('false => true', [
        animate('200ms ease-in', style({opacity: 1, transform: 'scale(1.03)'})),
        animate('150ms ease-out', style({transform: 'scale(1.06)'})),
        animate('200ms ease-out', style({transform: 'scale(1)'})),
      ]),
    ]),
    trigger('initialScreen', [
      state('true', style({opacity: 1})),
      state('false', style({opacity: 0})),
      transition('true => false', [
        animate('400ms ease-in-out')
      ]),
    ]),
    trigger('winnerScreen', [
      state('false', style({width: '0vw', left: '50%', height: '0vh', top: '50%'})),
      state('true', style({width: '100vw', left: '0%', height: '40vh', top: '30%'})),
      transition('false => true', [
        animate('1ms', style({backgroundColor: '#fff'})),
        animate('500ms ease-in', style({height: '1px', left: '0%', width: '100vw', top: '50%', backgroundColor: '#fff'})),
        animate('1ms', style({backgroundColor: 'inherit'})),
        animate('700ms ease-out', style({height: '40vh', left: '0%', width: '100vw', top: '30%'})),
      ]),
    ]),
    trigger('winnerScreenText', [
      state('true', style({opacity: 1, top: '20%'})),
      state('false', style({opacity: 0, top: '20%'})),
      transition('false => true', [
        animate('200ms ease-in-out')
      ]),
    ]),
    trigger('winnerName', [
      state('true', style({opacity: 1, top: '35%'})),
      state('false', style({opacity: 0, top: '35%'})),
      transition('false => true', [
        animate('200ms ease-in', style({opacity: 1, transform: 'scale(1.03)'})),
        animate('150ms ease-out', style({transform: 'scale(1.06)'})),
        animate('200ms ease-out', style({transform: 'scale(1)'})),
      ]),
    ]),
    trigger('winnerScreenTextOnResult', [
      state('true', style({top: '3%', transform: 'scale(0.7)'})),
      state('false', style({})),
      transition('false => true', [
        animate('500ms ease-in-out'),
      ]),
    ]),
    trigger('winnerNameOnResult', [
      state('true', style({top: '10%', transform: 'scale(0.7)'})),
      state('false', style({})),
      transition('false => true', [
        animate('500ms ease-in-out'),
      ]),
    ]),
    trigger('result', [
      state('true', style({top: '35%'})),
      state('false', style({top: '35%', opacity: 0})),
      transition('false => true', [
        animate('300ms', style({opacity: 0})),
        animate('500ms ease-in-out', style({opacity: 1}))
      ]),
    ]),
    trigger('twoPlayerLeft', [
      transition(':enter', [
        style({right: '200%', top: '25%', minHeight: '10vh'}),
        animate('1500ms ease-out'),
      ]),
    ]),
    trigger('twoPlayerRight', [
      transition(':enter', [
        style({left: '200%', top: '65%', minHeight: '10vh'}),
        animate('1500ms ease-out'),
      ]),
    ]),
    trigger('fourPlayerTopLeft', [
      state('void', style({left: '-100%', top: '25%', minHeight: '10vh'})),
      transition(':enter', [
        animate('1500ms ease-out'),
      ]),
    ]),
    trigger('fourPlayerTopRight', [
      state('void', style({right: '-100%', top: '25%', minHeight: '10vh'})),
      transition(':enter', [
        animate('1500ms ease-out'),
      ]),
    ]),
    trigger('fourPlayerBottomLeft', [
      state('void', style({left: '-100%', top: '65%', minHeight: '10vh'})),
      transition(':enter', [
        animate('1500ms ease-out'),
      ]),
    ]),
    trigger('fourPlayerBottomRight', [
      state('void', style({right: '-100%', top: '65%', minHeight: '10vh'})),
      transition(':enter', [
        animate('1500ms ease-out'),
      ]),
    ])
  ]
})
export class MatchResultComponent extends ComponentBase implements OnInit {
  @Input()
  message: PipelineEntryMatchResult;

  public textShadowProperty = null;
  public boxShadowProperty = null;

  public winner: Team;
  public losers: Team[];

  public showInitialScreen = true;
  public showVersus = false;
  public showWinnerScreen = false;
  public showWinnerScreenText = false;
  public showWinnerName = false;
  public showResult = false;

  constructor() {
    super();
  }

  ngOnInit() {
    this.textShadowProperty = {
      // tslint:disable-next-line:max-line-length
      textShadow: `0 0 10px #FFFFFF,0 0 20px #FFFFFF,0 0 30px #FFFFFF,0 0 40px ${this.visual.accentColor},0 0 70px ${this.visual.accentColor}`
    };
    this.boxShadowProperty = {
      // tslint:disable-next-line:max-line-length
      boxShadow: `0 0 2px #FFFFFF, 0 0 5px ${this.visual.accentColor}`
    };

    // this.message.payload.match.teams.push({name: 'Team 3', points: 1});
    // this.message.payload.match.teams.push({name: 'Team 4', points: 0});

    if (this.message.payload.match.teams.length === 2) {

      if (this.message.payload.match.teams[0].points > this.message.payload.match.teams[1].points) {

        this.winner = this.message.payload.match.teams[0];
        this.losers = [this.message.payload.match.teams[1]];

      } else {

        this.winner = this.message.payload.match.teams[1];
        this.losers = [this.message.payload.match.teams[0]];

      }

    } else {

      this.winner = this.message.payload.match.teams[0];
      this.message.payload.match.teams.forEach((team) => {
        if (this.winner.points < team.points) {
          this.winner = team;
        }
      });

      this.losers = this.message.payload.match.teams
        .filter(t => t.name !== this.winner.name)
        .sort((a, b) => a.points < b.points ? 1 : a.points === b.points ? 0 : -1);
    }

    setTimeout(() => {
      this.showVersus = true;
    }, 2000);

    setTimeout(() => {
      this.showWinnerScreen = true;
      this.showInitialScreen = false;
    }, 10000);

    setTimeout(() => {
      this.showWinnerScreenText = true;
      this.showWinnerName = true;
    }, 11200);

    setTimeout(() => {
      this.showResult = true;
    }, 18000);
  }

}
