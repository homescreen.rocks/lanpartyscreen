import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {environment} from '../../../environments/environment';
import {ComponentBase} from '../../classes/component-base/component-base';
import {Match} from '../../classes/match';
import {PipelineEntryTournament} from '../../classes/pipeline-entry-tournament';
import {Tournament} from '../../classes/tournament';

@Component({
  selector: 'app-upcoming-matches',
  templateUrl: './upcoming-matches.component.html',
  styleUrls: ['./upcoming-matches.component.scss'],
})
export class UpcomingMatchesComponent extends ComponentBase implements OnChanges {

  @Input() message: PipelineEntryTournament;
  tournament: Tournament;
  matchList: Match[] = [];


  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.tournament = changes['message'].currentValue.payload;
    // only completed matches
    const list = <Match[]>this.tournament.matches.filter(match => {
      return match.teams.filter(team => {
        return team.points === 0;
      }).length === match.teams.length;
    });
    setTimeout(() => {
      this.startTableAnimation(list);
    }, environment.enterAnimationTimeout);
  }

  private startTableAnimation(list: Match[]) {
    list.forEach((match: Match, index) => {
      if (index < 8) {
        setTimeout(() => {
          this.matchList.push(match);
        }, 500 * (index + 1) / 3);
      }
    });
  }

}
