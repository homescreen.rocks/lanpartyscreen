import {animate, style, transition, trigger} from '@angular/animations';
import {Component, Input, OnInit} from '@angular/core';
import {ComponentBase} from '../../classes/component-base/component-base';
import {PipelineEntryAnnouncement} from '../../classes/pipeline-entry-announcement';

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.scss'],
  animations: [
    trigger('container', [
      transition(':enter', [
        style({bottom: '-200%', minHeight: '100vh'}),
        animate('1500ms ease-out'), style({bottom: '0%'})
      ]),
    ])
  ]
})
export class AnnouncementsComponent extends ComponentBase implements OnInit {
  @Input()
  message: PipelineEntryAnnouncement;

  constructor() {
    super();
  }

  ngOnInit() {
    setTimeout(() => this.message.isNew = false, 7000);
  }

}
