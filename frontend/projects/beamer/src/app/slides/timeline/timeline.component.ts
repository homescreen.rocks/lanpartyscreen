import {Component, Input, OnInit} from '@angular/core';
import {PipelineEntryTimeline} from '../../classes/pipeline-entry-timeline';
import {TimelineEntry} from '../../classes/timeline-entry';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  @Input()
  message: PipelineEntryTimeline;

  data: TimelineEntry[] = [];

  constructor() {
  }

  ngOnInit() {
    Object.keys(this.message.payload).sort().forEach(key => {
      this.message.payload[key].forEach((e: TimelineEntry) => {
        this.data.push(e);
      });

    });
  }
}
