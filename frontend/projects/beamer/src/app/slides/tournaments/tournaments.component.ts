import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {environment} from '../../../environments/environment';
import {ComponentBase} from '../../classes/component-base/component-base';
import {PipelineEntryTournament} from '../../classes/pipeline-entry-tournament';
import {Tournament} from '../../classes/tournament';

@Component({
  selector: 'app-tournaments',
  templateUrl: './tournaments.component.html',
  styleUrls: ['./tournaments.component.scss']
})
export class TournamentsComponent extends ComponentBase implements OnChanges {
  @Input()
  message: PipelineEntryTournament;

  public shownTournamentList: Tournament[] = [];
  public firstShownIndex = 0;
  private cycledTournamentList: Tournament[] = [];

  private maxListLength = 7;

  constructor() {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.cycledTournamentList = [...changes['message'].currentValue.payload];
    setTimeout(() => {
      this.startTableAnimation(this.cycledTournamentList);
    }, environment.enterAnimationTimeout);
  }

  private startTableAnimation(list) {
    list.forEach((tournament: Tournament, index) => {
      if (index < this.maxListLength) {
        setTimeout(() => {
          this.shownTournamentList.push(tournament);
        }, 500 * (index + 1) / 3);
      } else if (index === list.length - 1) {
        this.cycleThroughEntries();
      }
    });
  }

  private cycleThroughEntries() {
    setTimeout(() => {
      // extends the cycled list by payload if end is reached
      if (this.firstShownIndex + this.maxListLength > this.shownTournamentList.length - 1) {
        this.cycledTournamentList = this.cycledTournamentList.concat(...this.message.payload);
      }
      // add next item to view
      this.shownTournamentList.push(this.cycledTournamentList[this.firstShownIndex + this.maxListLength]);
      // start out-animation fir first shown item
      this.firstShownIndex++;
      this.cycleThroughEntries();
    }, 5000);
  }


}
