import {Component, Input, OnInit} from '@angular/core';
import {PipelineEntryPicture} from '../../classes/pipeline-entry-picture';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.scss']
})
export class PicturesComponent implements OnInit {

  @Input()
  message: PipelineEntryPicture;

  constructor() {
  }

  ngOnInit() {
  }

}
