import {Component, Input, OnInit} from '@angular/core';
import {ComponentBase} from '../../classes/component-base/component-base';
import {PipelineEntryWelcomeMessage} from '../../classes/pipeline-entry-welcome-message';


@Component({
  selector: 'app-welcome-message',
  templateUrl: './welcome-message.component.html',
  styleUrls: ['./welcome-message.component.scss']
})
export class WelcomeMessageComponent extends ComponentBase implements OnInit {
  @Input()
  message: PipelineEntryWelcomeMessage;

  public shadowProperty = null;

  constructor() {
    super();
  }

  ngOnInit() {
    this.shadowProperty = {
      // tslint:disable-next-line:max-line-length
      textShadow: `0 0 10px #FFFFFF,0 0 20px #FFFFFF,0 0 30px #FFFFFF,0 0 40px ${this.visual.accentColor},0 0 70px ${this.visual.accentColor}`
    };

  }

}
