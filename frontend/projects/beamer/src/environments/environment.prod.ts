export const environment = {
  production: true,
  showDebugJson: false,
  enterAnimationTimeout: 1500
};
